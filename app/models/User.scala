package models

case class User(id: Long, username: String, password: String)

object User {
    var users = Set(
        User(1, "johndamilola", "damilola123"),
        User(2, "dansmog", "daniel123")
    )

    def findAll = users.toList.sortBy(_.id)

    def findById(id: Long) = users.find(_.id == id)
}