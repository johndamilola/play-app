package controllers

import javax.inject._
import play.api._
import play.api.mvc._

import models.User

@Singleton
class HomeController @Inject() extends Controller {

  def index = Action { implicit request =>
    Ok(views.html.index())
  }

  def listAll = Action { implicit request =>
    val users = User.findAll
    Ok(views.html.list(users))
  }

  def show(id: Long) = Action { implicit request =>
    User.findById(id).map { user =>
      Ok(views.html.show(user))
    }.getOrElse(NotFound)
  }

  def getLogin = Action {
    Ok(views.html.auth.login())
  }

  def getRegister = Action {
    Ok(views.html.auth.register())
  }

}
